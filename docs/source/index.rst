.. git-option3 documentation master file, created by
   sphinx-quickstart on Mon Aug 20 22:43:21 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

git-option3
=======================================
A simple Git tutorial

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   step-1
   step-2
   step-3
   step-4
   ...
