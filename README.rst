Option 3 - Git Tutorial
=======================
This is a basic tutorial for Git users to get them used to the branching model
we intend to follow at Shakti. This is a moderate tutorial for novice Git
users and definitely a piece of cake for advanced Git users.
I have called this tutorial and the model *Option - 3*. It is merely an amalgamation
of whatever I picked up from the following existing models available on the Internet.

- `A successful branching model <https://nvie.com/posts/a-successful-git-branching-model/>`_
- `A successful branching model considered harmful <http://endoflineblog.com/gitflow-considered-harmful>`_
- `Practical Approach to Large Scale Agile Development <https://www.youtube.com/watch?v=2QGYEwghRSM&t=2063s>`_
- `Mainline Model - Perfoce <https://www.perforce.com/video-tutorials/mainline-model>`_
